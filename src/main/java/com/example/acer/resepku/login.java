package com.example.acer.resepku;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class login extends AppCompatActivity {
    EditText editPassword, editEmail;
    Button Login, daftar;
    FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        editEmail = (EditText) findViewById(R.id.txtEmailLogin);
        editPassword = (EditText) findViewById(R.id.txtPasswordLogin);
        Login = (Button) findViewById(R.id.Login);
        daftar = (Button) findViewById(R.id.daftar);
        mAuth = FirebaseAuth.getInstance();
        mAuth.addAuthStateListener(new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                if (firebaseAuth.getCurrentUser()!=null) {
                    // Sign in success, update UI with the signed-in user's information
                    Log.d("Adi", "signInWithEmail:success");
                    Intent intent = new Intent(login.this, homepage.class);
                    startActivity(intent);
                    finish();
                } else {
                    // If sign in fails, display a message to the user.
                    Log.w("adi", "signInWithEmail:failure");
//                    Toast.makeText(login.this, "Authentication failed.",
//                            Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    public void login(View view) {
        mAuth.signInWithEmailAndPassword(editEmail.getText().toString(), editPassword.getText().toString());
    }

    public void Daftar(View view) {
        Intent intent = new Intent(login.this, register.class);
        startActivity(intent);
    }
}


