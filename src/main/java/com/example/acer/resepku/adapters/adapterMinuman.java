package com.example.acer.resepku.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.acer.resepku.R;
import com.example.acer.resepku.model.modelMinuman;

import java.util.ArrayList;

public class adapterMinuman extends RecyclerView.Adapter<adapterMinuman.viewHolder>{
    private ArrayList<modelMinuman> listData = new ArrayList<>();
    itemClicked listener;

    public adapterMinuman(itemClicked listener) {
        this.listener = listener;
    }

    @NonNull
    @Override
    public viewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new viewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_minuman, viewGroup,false));
    }

    @Override
    public void onBindViewHolder(@NonNull viewHolder viewHolder, final int position) {
        viewHolder.txtminuman.setText(listData.get(position).getNama());
        Glide.with(viewHolder.itemView.getContext()).load(listData.get(position).getGambar()).into(viewHolder.gambarminuman);
        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemClicked(listData.get(position));
            }
        });
    }

    @Override
    public int getItemCount() {
        return listData.size();
    }

    public void addData (modelMinuman data){
        listData.add(data);
        notifyDataSetChanged();
    }

    public interface itemClicked{
        void onItemClicked(modelMinuman model);
    }

    class  viewHolder extends RecyclerView.ViewHolder {
        TextView txtminuman;
        ImageView gambarminuman;

        public viewHolder(@NonNull View itemView) {
            super(itemView);
            txtminuman = itemView.findViewById(R.id.txtminuman);
            gambarminuman =itemView.findViewById(R.id.gambarminuman);
        }
    }

}

