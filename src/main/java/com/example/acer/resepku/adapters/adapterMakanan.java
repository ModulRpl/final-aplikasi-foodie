package com.example.acer.resepku.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.acer.resepku.R;
import com.example.acer.resepku.model.modelMakanan;

import java.util.ArrayList;

public class adapterMakanan extends RecyclerView.Adapter<adapterMakanan.viewHolder>{
    private ArrayList<modelMakanan> listData = new ArrayList<>();
    itemClicked listener;

    public adapterMakanan(itemClicked listener) {
        this.listener = listener;
    }

    @NonNull
    @Override
    public viewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new viewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_makanan, viewGroup,false));
    }

    @Override
    public void onBindViewHolder(@NonNull viewHolder viewHolder, final int position) {
        viewHolder.txtmakanan.setText(listData.get(position).getNama());
        Glide.with(viewHolder.itemView.getContext()).load(listData.get(position).gambar).into(viewHolder.gambarmakanan);
        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemClicked(listData.get(position));
            }
        });
    }

    @Override
    public int getItemCount() {
        return listData.size();
    }

    public void addData (modelMakanan data){
        listData.add(data);
        notifyDataSetChanged();
    }

    public interface itemClicked{
        void onItemClicked(modelMakanan model);
    }

    class  viewHolder extends RecyclerView.ViewHolder {
        TextView txtmakanan;
        ImageView gambarmakanan;

        public viewHolder(@NonNull View itemView) {
            super(itemView);
            txtmakanan = itemView.findViewById(R.id.txtmakanan);
            gambarmakanan =itemView.findViewById(R.id.gambarmakanan);
        }
    }

}

