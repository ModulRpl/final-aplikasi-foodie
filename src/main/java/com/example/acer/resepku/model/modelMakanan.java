package com.example.acer.resepku.model;

public class modelMakanan {
    public String nama;
    public String gambar;
    public String resep;

    public modelMakanan(String nama, String gambar, String resep) {
        this.nama = nama;
        this.gambar = gambar;
        this.resep = resep;
    }

    public modelMakanan(){

    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getGambar() {
        return gambar;
    }

    public void setGambar(String gambar) {
        this.gambar = gambar;
    }

    public String getResep() {
        return resep;
    }

    public void setResep(String resep) {
        this.resep = resep;
    }
}
