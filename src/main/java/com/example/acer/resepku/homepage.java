package com.example.acer.resepku;

import android.content.Intent;
import android.media.Image;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;

import java.util.ArrayList;

public class homepage extends AppCompatActivity {
    ImageView makanann;
    ImageView minuman;
    ImageView about;
    ImageView tutorial;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_homepage);

        Intent intent= getIntent();

        makanann = (ImageView)findViewById(R.id.makanann);
        minuman = (ImageView)findViewById(R.id.minuman);
        about = (ImageView)findViewById(R.id.about);
        tutorial = (ImageView)findViewById(R.id.tutorial);

        makanann.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(homepage.this,makanan.class);
                homepage.this.startActivity(intent);
            }
        });
        minuman.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(homepage.this,minuman2.class);
                homepage.this.startActivity(intent);
            }
        });


        ((ImageView)findViewById(R.id.resep)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(homepage.this, TulisResep.class));
            }
        });
        about.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(homepage.this,About.class);
                homepage.this.startActivity(intent);
            }
        });
        tutorial.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(homepage.this,Tutorial.class);
                homepage.this.startActivity(intent);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.homepage, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.menu_logout:
                FirebaseAuth.getInstance().signOut();
                Intent intent = new Intent(homepage.this,login.class);
                homepage.this.startActivity(intent);
                Toast.makeText(homepage.this, "Anda Berhasil Logout..",
                        Toast.LENGTH_SHORT).show();
                break;
            case R.id.menu_settings:
                Toast.makeText(homepage.this, "Anda Sudah Selesai Melakukan Setting..",
                        Toast.LENGTH_SHORT).show();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}