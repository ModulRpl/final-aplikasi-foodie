package com.example.acer.resepku;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.example.acer.resepku.model.modelMakanan;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.UUID;

public class TulisResep extends AppCompatActivity {
    Button btntambah;
    Uri selectedImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tulis_resep);
        btntambah = (Button) findViewById(R.id.btntambah);

        final DatabaseReference dataMakanan = FirebaseDatabase.getInstance().getReference().child("makanan");
        final DatabaseReference dataMinuman = FirebaseDatabase.getInstance().getReference().child("minuman");
        final StorageReference storef = FirebaseStorage.getInstance().getReference();

        final ArrayList<modelMakanan> listdata = new ArrayList<>();

        btntambah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((ImageView)findViewById(R.id.gambar)).setDrawingCacheEnabled(true);
                ((ImageView)findViewById(R.id.gambar)).buildDrawingCache();
                Bitmap bitmap = ((BitmapDrawable) ((ImageView)findViewById(R.id.gambar)).getDrawable()).getBitmap();
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                byte[] data = baos.toByteArray();


                final StorageReference childRef = storef.child(UUID.randomUUID().toString()+".jpg");
                Log.d("HEHE", childRef.getDownloadUrl().toString());

                final UploadTask task = childRef.putFile(selectedImage);

                final String something  = "";

                task.addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                        Log.d("SOMETHING", taskSnapshot.getMetadata().getReference().getDownloadUrl().toString());

                        childRef.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                            @Override
                            public void onSuccess(Uri uri) {
                                modelMakanan baru = new modelMakanan();
                                baru.setNama(((EditText)findViewById(R.id.namatambahan)).getText().toString());
                                baru.setResep( ((EditText)findViewById(R.id.reseptambahan)).getText().toString() );
                                RadioGroup rg = findViewById(R.id.groupradio);
                                baru.setGambar(uri.toString());

                                String tipe = ((RadioButton)findViewById(rg.getCheckedRadioButtonId())).getText().toString();

                                if (tipe.equals("Makanan")){
                                    dataMakanan.push().setValue(baru).addOnSuccessListener(new OnSuccessListener<Void>() {
                                        @Override
                                        public void onSuccess(Void aVoid) {
                                            Toast.makeText(getApplicationContext(), "data Makanan berhasil ditambah", Toast.LENGTH_SHORT).show();
                                        }
                                    });
                                }else {
                                    dataMinuman.push().setValue(baru).addOnSuccessListener(new OnSuccessListener<Void>() {
                                        @Override
                                        public void onSuccess(Void aVoid) {
                                            Toast.makeText(getApplicationContext(), "data Minuman berhasil ditambah", Toast.LENGTH_SHORT).show();
                                        }
                                    });
                                }
                            }
                        });
                    }
                });
                finish();
            }
        });
    }

    public void openGallery(View view) {
        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
        photoPickerIntent.setType("image/*");
        startActivityForResult(photoPickerIntent, 9001);
    }

    @Override
    protected void onActivityResult(int reqCode, int resultCode, Intent data) {
        super.onActivityResult(reqCode, resultCode, data);
        if (resultCode == RESULT_OK&& reqCode==9001) {
            try {
                selectedImage = data.getData();
                final InputStream imageStream = getContentResolver().openInputStream(selectedImage);
                final Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);
                ((ImageView)findViewById(R.id.gambar)).setImageBitmap(selectedImage);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
                Toast.makeText(TulisResep.this, "Something went wrong", Toast.LENGTH_LONG).show();
            }

        }else {
            Toast.makeText(TulisResep.this, "You haven't picked Image",Toast.LENGTH_LONG).show();
        }
    }
}