package com.example.acer.resepku;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.example.acer.resepku.adapters.adapterMinuman;
import com.example.acer.resepku.model.modelMinuman;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class minuman2 extends AppCompatActivity {
    adapterMinuman adapter;
    RecyclerView data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_minuman2);
        adapter = new adapterMinuman(new adapterMinuman.itemClicked() {
            @Override
            public void onItemClicked(modelMinuman model) {
                Intent intent = new Intent(minuman2.this, drink.class);

                intent.putExtra("nama", model.nama);
                intent.putExtra("gambar", model.gambar);
                intent.putExtra("ingredient", model.resep);

                minuman2.this.startActivity(intent);
            }
        });

        data = findViewById(R.id.list_minuman);
        data.setLayoutManager(new LinearLayoutManager(this));
        data.addItemDecoration(new DividerItemDecoration(data.getContext(), DividerItemDecoration.VERTICAL));
        data.setHasFixedSize(true);
        data.setAdapter(adapter);

        DatabaseReference reference = FirebaseDatabase.getInstance().getReference().child("minuman");
        reference.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                modelMinuman baru = dataSnapshot.getValue(modelMinuman.class);
                adapter.addData(baru);
            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


    }
}