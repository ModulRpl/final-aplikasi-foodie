package com.example.acer.resepku;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

public class drink extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drink);

        getSupportActionBar().setTitle(getIntent().getStringExtra("nama"));
        Glide.with(this).load(getIntent().getStringExtra("gambar")).into(((ImageView)findViewById(R.id.gambar)));
        ((TextView)findViewById(R.id.articledrink)).setText(getIntent().getStringExtra("ingredient"));
    }
}

